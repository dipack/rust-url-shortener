use core::{connect_to_db, generate_random_alphanumeric, GEN_SHORTCODE_LEN};
use sqlx::{Executor, Row, SqliteConnection};
use std::collections::HashMap;
use std::error::Error;
use url::{ParseError, Url};

fn parse_args() -> Result<Vec<String>, ParseError> {
    let mut args: Vec<String> = std::env::args().collect();
    if args.is_empty() {
        return Ok(args);
    }
    args.remove(0);
    let mut urls: Vec<String> = Vec::new();
    for url in args {
        let maybe_url = Url::parse(&url);
        let host = match &maybe_url {
            Ok(u) => u.host(),
            Err(err) => panic!("Argument: '{}' Error: {}", url, err),
        };
        urls.push(host.unwrap().to_string());
    }
    Ok(urls)
}

async fn seed_db(conn: &mut SqliteConnection) -> Result<(), sqlx::Error> {
    let seed_urls = ["dipack.dev", "google.xyz", "arstechnica.com", "wired.com"];
    let mut queries: Vec<String> = vec![
        String::from("DROP TABLE IF EXISTS urls ;"),
        String::from(
            "CREATE TABLE IF NOT EXISTS urls (
            url text PRIMARY KEY,
            shortcode text NOT NULL
        ) ;",
        ),
        String::from("CREATE INDEX idx_urls_shortcode ON urls (shortcode) ;"),
    ];

    for url in seed_urls {
        queries.push(format!(
            "INSERT INTO urls (url, shortcode) VALUES ( \"{}\", \"{}\" );",
            url,
            generate_random_alphanumeric(GEN_SHORTCODE_LEN)
        ))
    }
    for query in queries {
        conn.execute(sqlx::query(query.as_str())).await?;
    }
    Ok(())
}

async fn assert_db(conn: &mut SqliteConnection) -> Result<(), Box<dyn Error>> {
    let result = conn.execute(sqlx::query("SELECT * FROM urls")).await?;
    if result.rows_affected() == 0 {
        panic!("Expected to find at least one row in the urls table");
    }
    Ok(())
}

async fn print_db_table(conn: &mut SqliteConnection) -> Result<(), sqlx::Error> {
    let rows = conn.fetch_all(sqlx::query("SELECT * FROM urls")).await?;
    for row in rows {
        let url: String = row.get("url");
        let shortcode: String = row.get("shortcode");
        println!("URL: {}, Shortcode: {}", url, shortcode);
    }
    Ok(())
}

async fn find_and_generate_shortcodes(
    conn: &mut SqliteConnection,
    inputs: Vec<String>,
) -> Result<HashMap<String, String>, Box<dyn Error>> {
    let mut url_to_shortcode: HashMap<String, String> = HashMap::new();
    let mut new_urls_to_shortcodes: HashMap<String, String> = HashMap::new();

    let param = format!("'{}'", inputs.join("', '"));
    let formatted_query = format!(
        "SELECT url, shortcode FROM urls WHERE url IN ( {} );",
        param
    );
    let query = sqlx::query(&formatted_query);

    let rows = conn.fetch_all(query).await?;

    for row in rows {
        let url: String = row.get("url");
        let shortcode: String = row.get("shortcode");
        url_to_shortcode.insert(url, shortcode);
    }

    for input in inputs {
        // If we don't have the URL in our DB already.
        if !url_to_shortcode.contains_key(&input) {
            let new_shortcode = generate_random_alphanumeric(GEN_SHORTCODE_LEN);
            new_urls_to_shortcodes.insert(input, new_shortcode);
        }
    }

    let mut inserts: Vec<String> = Vec::new();
    for (new_url, new_shortcode) in new_urls_to_shortcodes {
        inserts.push(format!(" ('{}', '{}' )", new_url, new_shortcode));
        url_to_shortcode.insert(new_url, new_shortcode);
    }

    let insert_sql = format!(
        "INSERT INTO urls (url, shortcode) VALUES {} ;",
        inserts.join(", ")
    );
    conn.execute(sqlx::query(insert_sql.as_str())).await?;

    Ok(url_to_shortcode)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let urls = parse_args()?;
    let mut connection = connect_to_db().await?;
    seed_db(&mut connection).await?;
    assert_db(&mut connection).await?;
    if !urls.is_empty() {
        println!("========== BEFORE ==========");
        print_db_table(&mut connection).await?;
        println!("========== BEFORE ==========");

        find_and_generate_shortcodes(&mut connection, urls).await?;

        println!("========== AFTER ==========");
        print_db_table(&mut connection).await?;
        println!("========== AFTER ==========");
    }
    Ok(())
}
