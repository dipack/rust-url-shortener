use rand::distributions::{Alphanumeric, DistString};
use sqlx::{Connection, SqliteConnection};

pub const GEN_SHORTCODE_LEN: usize = 8;

pub fn generate_random_alphanumeric(length: usize) -> String {
    Alphanumeric.sample_string(&mut rand::thread_rng(), length)
}

pub async fn connect_to_db() -> Result<SqliteConnection, sqlx::Error> {
    let connection = SqliteConnection::connect("sqlite://urls.db").await?;
    Ok(connection)
}
