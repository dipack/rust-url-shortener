use actix_web::{get, web, App, HttpResponse, HttpServer};
use core::{connect_to_db, generate_random_alphanumeric, GEN_SHORTCODE_LEN};
use serde::Deserialize;
use sqlx::{Executor, Row};
use url::Url;

#[get("/all")]
async fn list_all() -> HttpResponse {
    let mut connection = connect_to_db().await.unwrap();
    let formatted_query = format!("SELECT * FROM urls ;",);
    let rows = connection
        .fetch_all(sqlx::query(formatted_query.as_str()))
        .await;
    if rows.is_err() {
        println!("Error {}", rows.err().unwrap());
        return HttpResponse::InternalServerError().finish();
    }
    let mut body = String::from("<div>");
    for row in rows.unwrap() {
        let url: String = row.get("url");
        let shortcode: String = row.get("shortcode");
        body.push_str(&format!("<p>URL: {}, Shortcode: {}</p>", url, shortcode));
    }
    body.push_str(&"</div>");
    HttpResponse::Ok().body(body)
}

#[derive(Deserialize, Debug)]
struct GoParameters {
    url: String,
}

#[get("/go")]
async fn create_and_redirect(params: web::Query<GoParameters>) -> HttpResponse {
    let maybe_url = Url::parse(&params.url);
    if maybe_url.is_err() {
        return HttpResponse::BadRequest().body(format!("{} is not a valid URL", params.url));
    }
    let url = maybe_url.unwrap();
    let mut connection = connect_to_db().await.unwrap();
    let maybe_row = connection
        .fetch_one(sqlx::query(
            format!(
                "SELECT COUNT(url) as \"count\" FROM urls WHERE url = '{}'",
                url.host().unwrap().to_string()
            )
            .as_str(),
        ))
        .await;
    if maybe_row.is_err() {
        return HttpResponse::InternalServerError().body(format!(
            "Server error: {}",
            maybe_row.err().unwrap().to_string()
        ));
    }
    let count = maybe_row.unwrap().get::<u32, &str>("count");
    if count > 0 {
        return HttpResponse::TemporaryRedirect()
            .append_header(("Location", url.to_string()))
            .finish();
    }
    let shortcode = generate_random_alphanumeric(GEN_SHORTCODE_LEN);
    let insert_query = connection
        .execute(sqlx::query(
            format!(
                "INSERT INTO urls (url, shortcode) VALUES ( '{}', '{}' ); ",
                url.host().unwrap().to_string(),
                shortcode
            )
            .as_str(),
        ))
        .await;
    if insert_query.is_err() {
        return HttpResponse::InternalServerError().body(format!(
            "Failed to generate a shortcode for {} due to {}",
            url.host().unwrap().to_string(),
            insert_query.err().unwrap().to_string()
        ));
    }
    HttpResponse::TemporaryRedirect()
        .append_header(("Location", url.to_string()))
        .finish()
}

#[get("/{shortcode}")]
async fn redirect(shortcode: web::Path<String>) -> impl actix_web::Responder {
    let mut connection = connect_to_db().await.unwrap();
    let formatted_query = format!(
        "SELECT url, shortcode FROM urls WHERE shortcode = '{}'",
        shortcode
    );
    let maybe_row = connection
        .fetch_one(sqlx::query(formatted_query.as_str()))
        .await;
    if maybe_row.is_err() {
        return HttpResponse::NotFound().finish();
    }
    let url: String = format!("https://{}", maybe_row.unwrap().get::<String, &str>("url"));
    HttpResponse::TemporaryRedirect()
        .append_header(("Location", url))
        .finish()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(list_all)
            .service(create_and_redirect)
            .service(redirect)
    })
    .bind(("127.0.0.1", 8089))?
    .run()
    .await
}
